package ru.agilix.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.agilix.blog.model.Entry;

@Repository
public interface EntryRepository extends JpaRepository<Entry, Long> {
}
